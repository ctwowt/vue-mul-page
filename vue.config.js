/*
 * @Author: hucheng
 * @Date: 2020-05-10 17:05:56
 * @Description: here is des
 */
/* eslint-disable */
const path  = require("path");
const { getPageEntries } = require("./build/utils");

const { PAGE_NAME } = process.env;
console.log(getPageEntries(PAGE_NAME) );
module.exports = {
  pages: getPageEntries(PAGE_NAME),
  outputDir: path.resolve(__dirname, `./dist/${PAGE_NAME}`),
  publicPath:  `/${PAGE_NAME}/`,
  indexPath: "index.html",
  chainWebpack: config => {
    config.resolve
        .alias
        .set('@', path.posix.join(__dirname, `apps/${PAGE_NAME}/`))
   
},
devServer: {
  open: true,
  openPage: `pages/${PAGE_NAME}/index.html`
}
};