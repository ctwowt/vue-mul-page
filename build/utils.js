/*
 * @Author: hucheng
 * @Date: 2020-05-10 18:46:21
 * @Description: here is des
 */
const globby = require('globby');
const pattern = ['./apps/*/index.js'];
const { resolve } = require('path');

function getPageEntries (pageName) {
    const entries = {};
    const paths = globby.sync(pattern);
    paths.forEach(path => {
        const name = path.split('/')[2];
        if (name === pageName) {
            entries[name] = {
                entry: path,
                template: resolve(__dirname, '../public/index.html'),
                filename: 'index.html',
                chunks: ['chunk-vendors', 'chunk-common', pageName]
            };
            return false;
        }
    });
    console.log(entries);
    return entries;
}

function getPageNames () {
    return globby.sync(pattern).map(path => path.split('/')[2]);
}
module.exports = {
    getPageEntries,
    getPageNames
};
